import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {


  loginForm : FormGroup;

  error_messages = {
    'username':[
      { type:'required', message: 'Username is required'},
      { type:'minlength', message: 'Username lenght must be longer than or equal to 6 characters '},
      { type:'maxlength', message: 'Username lenght Cannot exceed 20 characters '},
      { type:'pattern', message: 'Please enter valid Username format '}
    ],
    'email':[
      { type:'required', message: 'Email is required'},
      { type:'minlength', message: 'Email lenght must be longer than or equal to 6 characters '},
      { type:'maxlength', message: 'Email lenght Cannot exceed 20 characters '},
      { type:'pattern', message: 'Please enter valid email format '}
    ],
    'password':[
      { type:'required', message: 'Password is required'},
      { type:'minlength', message: 'Password lenght must be longer than or equal to 6 characters '},
      { type:'maxlength', message: 'Password lenght Cannot exceed 10 characters '},
      { type:'pattern', message: 'Password must contain numbers,uppercase and lowercase letters '}
    ]
  }
  
  constructor(public formBuilder:FormBuilder,private navCtrl:NavController) {
    this.loginForm = this.formBuilder.group({
      username: new FormControl('',Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(20),
        Validators.pattern('^[a-zA-Z0-9_.+-]+$')
      ])),
      email: new FormControl('',Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(20),
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9.-]+.[a-zA-Z0-9.-]+$')
      ])),
      password: new FormControl('',Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(10),
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ]))
    })
   }

  ngOnInit() {
  }

  signup()
  {
    this.navCtrl.navigateRoot('tabs');
  }

}
