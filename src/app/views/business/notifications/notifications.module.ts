import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotificationsRoutingModule } from './notifications-routing.module';
import { NgxGaugeModule } from 'ngx-gauge';
import { NotificationsPage } from './notifications.page';

import { InfoModalPageModule } from '../modals/info-modal/info-modal.module';
import { InfoModalPage } from '../modals/info-modal/info-modal.page';
import { DakarisDatePipe } from 'src/app/util/pipes/dakaris-date.pipe';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    IonicModule,
    NotificationsRoutingModule,
    NgxGaugeModule,
    InfoModalPageModule
  ],
  entryComponents: [InfoModalPage],
  declarations: [NotificationsPage]
})
export class NotificationsPageModule {}