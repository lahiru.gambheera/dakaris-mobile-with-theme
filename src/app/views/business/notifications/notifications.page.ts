import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { MyNotification } from 'src/app/models/dto/my-notification.model';
import { Parameter } from 'src/app/util/model/api-service/parameter.model';
import { GlobalApiService } from 'src/app/util/services/global-api.service';
import { InfoModalPage } from '../modals/info-modal/info-modal.page';

@Component({
  selector: 'app-about',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  public myNotifications: Array<MyNotification> = [];

  constructor(
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    private apiService: GlobalApiService) { }

  ngOnInit() {
    this.getMyNotifications();
  }

  viewNotifications() {
    this.navCtrl.navigateForward('notifications');
  }

  onViewNotificationClick(message: string) {
    this.openFullNotificationModal(message);
  }

  private getMyNotifications() {
    const parameterList = new Array<Parameter>();

    parameterList.push({
      name: "page",
      value: "1"
    });

    parameterList.push({
      name: "pageSize",
      value: "100"
    });

    this.apiService.executeApiRequest('MyNotifications', parameterList).subscribe((res: Array<MyNotification>) => {
      console.log(res);

      this.myNotifications = res;
    });
  }

  private openFullNotificationModal(message: string) {
    this.modalCtrl.create({
      component: InfoModalPage,
      backdropDismiss: true,
      componentProps: {
        title: `Notification`,
        message1: message,
        button3Text: `OK`,
      }
    }).then(modal => {
      modal.present();
      modal.onDidDismiss().then(res => {
        if (res.data.isButton3Clicked) {
        }
      });
    });
  }

}
