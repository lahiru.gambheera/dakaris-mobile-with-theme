import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HospitalListCcPage } from './hospital-list-cc.page';


const routes: Routes = [
  {
    path: '',
    component: HospitalListCcPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HospitalListCcRoutingModule { }
