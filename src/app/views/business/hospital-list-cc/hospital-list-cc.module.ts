import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { HospitalListCcRoutingModule } from './hospital-list-cc-routing.module';
import { HospitalListCcPage } from './hospital-list-cc.page';


@NgModule({
  declarations: [HospitalListCcPage],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HospitalListCcRoutingModule
  ]
})
export class HospitalListCcModule { }
