import { GooglePlus } from '@ionic-native/google-plus/ngx';

import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Parameter } from 'src/app/util/model/api-service/parameter.model';
import { GlobalApiService } from 'src/app/util/services/global-api.service';
import { GoogleUser } from 'src/app/models/dto/google-user.model';
import { LoginSuccessUser } from 'src/app/models/dto/login-success-user.model';

import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {
  slideOpts = {
    autoplay: true,
    loop: true
  };

  constructor(
    private navCtrl: NavController,
    private googlePlus: GooglePlus,
    private apiService: GlobalApiService,
    private nativeStorage: NativeStorage) { }

  ngOnInit() {
  }

  loginWithGoogle() {
    debugger;
    this.googlePlus.login({
      // 496955827722-sa84u45gdqfpvejt6a2m7kabq1k0t34o.apps.googleusercontent.com

    })
      .then((res: any) => {
        console.log(res);
        const googleUser: GoogleUser = {};
        googleUser.email = res.email;
        googleUser.token = res.idToken;
        googleUser.userId = res.userId;
        googleUser.name = res.userId;

        // this.nativeStorage.setItem('auth_tokenddd', "wefwefwfwefwef").then(done => {
        this.nativeStorage.getItem('auth_token').then(tk => {
          debugger;
        }, err => {
          debugger;
        });
        // });

        this.confirmLogin(googleUser);
      })
      .catch(
        err => {
          ;
          console.error(err)
        }
      );

    // this.navCtrl.navigateRoot('tabs/tab1');
  }

  goSignup() {
    this.navCtrl.navigateRoot('signup');
  }

  private async confirmLogin(googleUser: GoogleUser) {
    const parameterList = new Array<Parameter>();

    parameterList.push({
      name: 'googleUser',
      value: googleUser
    });

    this.apiService.executeApiRequest('GoogleLogin', parameterList).subscribe((res: LoginSuccessUser) => {
      ;
      this.nativeStorage.setItem('auth_token', res.token);
      console.log('Token saved');

      ;

      // var x = await this.nativeStorage.getItem('auth_token')

      this.nativeStorage.getItem('auth_token')
        .then(
          data => console.log('Token: >>>', data),
          error => console.error(error)
        );

      this.navCtrl.navigateRoot('tabs/tab1');

      ;
    });
  }
}
