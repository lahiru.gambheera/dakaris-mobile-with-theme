import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-info-modal',
  templateUrl: './info-modal.page.html',
  styleUrls: ['./info-modal.page.scss'],
})
export class InfoModalPage implements OnInit {

  @Input() title = '';
  @Input() message1 = '';
  @Input() message2 = '';

  public button1Text = '';
  public button2Text = '';
  public button3Text = '';

  constructor(private modalCtrl: ModalController, public navCtrl: NavController) { }

  ngOnInit() {
  }

  dismiss() {
    this.modalCtrl.dismiss({
      dismissed: true,
      abc: 'erf'
    });
  }

  onButton1Clicked() {
    this.modalCtrl.dismiss({
      dismissed: true,
      isButton1Clicked: true,
      isButton2Clicked: false,
      isButton3Clicked: false
    });
  }

  onButton2Clicked() {
    this.modalCtrl.dismiss({
      dismissed: true,
      isButton1Clicked: false,
      isButton2Clicked: true,
      isButton3Clicked: false
    });
  }

  onButton3Clicked() {
    this.modalCtrl.dismiss({
      dismissed: true,
      isButton1Clicked: false,
      isButton2Clicked: false,
      isButton3Clicked: true
    });
  }

  setNavigation(param: string, url: string) {
    this.dismiss();
    const navigationExtras: NavigationExtras = {
      queryParams: {
        type: param
      }
    };

    this.navCtrl.navigateForward([url], navigationExtras);
  }

}
