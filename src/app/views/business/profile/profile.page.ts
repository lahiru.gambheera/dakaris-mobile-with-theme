import { NurseDto } from './../../../models/dto/nurse.model';
import { Component, OnInit } from '@angular/core';
import { GlobalApiService } from 'src/app/util/services/global-api.service';
import { ProfilePicService } from 'src/app/services/profile-pic.service';
import { District } from 'src/app/models/enum/district.enum';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  public myData: NurseDto = {};

  constructor(
    private navCtrl: NavController,
    private apiService: GlobalApiService,
    private profilePicService: ProfilePicService
  ) { }

  ngOnInit() {
    this.getMyData();
  }

  public getDistrictName(district: number): string {
    return District[district];
  }

  public navigateToEdit() {
    this.navCtrl.navigateForward('tabs/tab5/1');
  }

  private getMyData() {
    this.apiService.executeApiRequest('GetMyData', []).subscribe((res: NurseDto) => {
      console.log('My Profile Data', res);
      this.myData = res;
      this.myData.avatarUrl = this.profilePicService.getProfilePicUrl(res.encriptedId);
    });
  }
}
