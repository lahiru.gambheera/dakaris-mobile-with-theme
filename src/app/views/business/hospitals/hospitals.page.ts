import { District } from './../../../models/enum/district.enum';
import { Component, OnInit } from '@angular/core';
import { Hospital } from 'src/app/models/business/hospital.model';
import { HospitalMemoService } from 'src/app/services/hospital-memo.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-notifications',
  templateUrl: './hospitals.page.html',
  styleUrls: ['./hospitals.page.scss'],
})
export class HospitalsPage implements OnInit {

  public allHospitalList: Array<Hospital>;
  public searchKey = '';

  public fullDistrictSelection = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];

  constructor(
    private navCtrl: NavController,
    private hospitalMemoService: HospitalMemoService) { }

  ngOnInit() {
    this.allHospitalList = this.hospitalMemoService.getAllHospitalList();

    const selectedHospitalList = this.hospitalMemoService.getSelectedHospitalList();

    if (selectedHospitalList) {
      this.allHospitalList.map(h => h.isSelected = selectedHospitalList.find(sh => sh.id === h.id) ? true : false);
    }

    const disabledHospitalList = this.hospitalMemoService.getDisabledHospitalList();

    if (disabledHospitalList) {
      this.allHospitalList.map(h => h.isDisabled = disabledHospitalList.find(sh => sh.id === h.id) ? true : false);
    }
  }

  onHospitalSelectUnselect(hospital: Hospital) {
    if (this.hospitalMemoService.isInMultiSelectMode()) { return; }

    this.allHospitalList.filter(h => h.id !== hospital.id && hospital.isSelected).map(h => h.isSelected = false);
  }

  public getHospitalByDistrict(district: number): Array<Hospital> {
    return this.allHospitalList.filter(h => h.district === district);
  }

  public selectUnselectAllHospitals(district: number): void {
    this.fullDistrictSelection[district] = !this.fullDistrictSelection[district];
    this.allHospitalList.filter(h => h.district === district).forEach(h => {
      h.isSelected = this.fullDistrictSelection[district];
    });
  }

  public chackForFullDistrictSelection(district: number, hospital: Hospital) {
    hospital.isSelected = !hospital.isSelected;
    const selectedLength = this.allHospitalList.filter(h => h.district === district && h.isSelected).length;
    const fullLength = this.allHospitalList.filter(h => h.district === district).length;

    this.fullDistrictSelection[district] = selectedLength === fullLength;
  }

  public getDistrictName(district: number): string {
    return District[district];
  }

  public confirmSelection() {
    const selectedHospitalList = this.allHospitalList.filter(h => h.isSelected);
    this.hospitalMemoService.updatedCurrentHospitalDataSelection(selectedHospitalList);
    if (this.hospitalMemoService.getIsInEditState()) {
      this.navCtrl.navigateBack(['tabs', 'tab4', '1']);
    } else {
      this.navCtrl.navigateBack(['tabs', 'tab4', '0']);
    }


  }
}
