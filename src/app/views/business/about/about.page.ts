import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {


  category:any = "day";
  gaugeLabel = "of $3824";
  gaugePrependText = "$"
  gaugeType = "arch";
  gaugeValue = 50.5;
 
  

  constructor(private navCtrl:NavController) { }

  ngOnInit() {
  }

  segmentChanged(ev: any) {
    this.category = ev.detail.value;
    console.log(this.category);
  }

  viewNotifications()
  {
    this.navCtrl.navigateForward('notifications');
  }

}
