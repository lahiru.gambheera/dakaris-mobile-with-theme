import { Hospital } from 'src/app/models/business/hospital.model';
import { NurseDto } from './../../../models/dto/nurse.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ModalController, NavController } from '@ionic/angular';
import { HospitalDto } from 'src/app/models/dto/hospitalDto.model';
import { HospitalMemoService } from 'src/app/services/hospital-memo.service';
import { Parameter } from 'src/app/util/model/api-service/parameter.model';
import { GlobalApiService } from 'src/app/util/services/global-api.service';
import { Location } from '@angular/common';
import { forkJoin } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { SuccessmodalPage } from '../modals/successmodal/successmodal.page';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  public signupForm: FormGroup;

  private hospitalDtoList: Array<HospitalDto>;
  private hospitalList: Array<Hospital>;
  private myData: NurseDto = {};

  private isInEdit = true;

  public isAgreed = false;

  error_messages = {
    username: [
      { type: 'required', message: 'Username is required' },
      { type: 'minlength', message: 'Username lenght must be longer than or equal to 6 characters ' },
      { type: 'maxlength', message: 'Username lenght Cannot exceed 20 characters ' },
      { type: 'pattern', message: 'Please enter valid Username format ' }
    ],
    email: [
      { type: 'required', message: 'Email is required' },
      { type: 'minlength', message: 'Email lenght must be longer than or equal to 6 characters ' },
      { type: 'maxlength', message: 'Email lenght Cannot exceed 20 characters ' },
      { type: 'pattern', message: 'Please enter valid email format ' }
    ],
    password: [
      { type: 'required', message: 'Password is required' },
      { type: 'minlength', message: 'Password lenght must be longer than or equal to 6 characters ' },
      { type: 'maxlength', message: 'Password lenght Cannot exceed 10 characters ' },
      { type: 'pattern', message: 'Password must contain numbers,uppercase and lowercase letters ' }
    ]
  };

  constructor(
    private modalCtrl: ModalController,
    public formBuilder: FormBuilder,
    private navCtrl: NavController,
    private hospitalMemoService: HospitalMemoService,
    private apiService: GlobalApiService,
    private location: Location,
    private activatedRoute: ActivatedRoute) {

  }

  get signupFormControls() {
    return this.signupForm.controls;
  }

  ngOnInit() {
    // this.getAllHospitals();
    // this.getMyData();

    this.isInEdit = !(this.activatedRoute.snapshot.paramMap.get('isInEdit') === '0')

    this.hospitalMemoService.setIsInEditState(this.isInEdit);

    this.initializeData()

    this.hospitalMemoService.selectedCurrentHospitaldata.subscribe(data => {
      this.manageDataFromHospitalList(data);
    });



    this.initiateForm();
  }

  signup() {
    if (!this.signupForm.valid) { return; }

    // this.navCtrl.navigateRoot('tabs');
    var values = this.signupFormControls;

    const nurse = {
      id: 0,
      encriptedId: "string",
      name: values.fullName.value,
      nic: values.nic.value,
      mobileNumber: values.mobileNumber.value,
      extraMobileNo: values.extraMobileNumber.value,
      email: values.email ? values.email.value : 'a@bc.com',
      registeredPromoCode: values.promoCode.value,
      currentHospitalId: this.myData.currentHospitalId,
      nurseExpectingHospitalList: this.myData.nurseExpectingHospitalList.map(h => h.id)
    } as NurseDto;

    const parameterList = new Array<Parameter>();
    parameterList.push({
      name: 'nurse',
      value: nurse
    });

    this.apiService.executeApiRequest('PostNurse', parameterList).subscribe((res: NurseDto) => {
      this.myData = res;
      this.showSuccess();
      console.log(res);
    });
  }

  viewHospitalListForCurrentHospital() {
    const hospital = this.myData.currentHospital as Hospital;
    if (hospital) { hospital.isSelected = true; }

    if (!this.myData || !this.myData.nurseExpectingHospitalList) { this.myData.nurseExpectingHospitalList = []; }

    const expectedHospitalList = this.myData.nurseExpectingHospitalList as Array<Hospital>;
    expectedHospitalList.map(h => h.isSelected = true);

    // const seletingHospitalList = expectedHospitalList;
    // if (hospital) { seletingHospitalList.push(hospital); }

    const r = this.hospitalMemoService.setMultiselectMode(false);
    const s = this.hospitalMemoService.setAllHospitalList(this.hospitalList);
    if (hospital) { const t = this.hospitalMemoService.setSelectedHospitalList([hospital]); }
    const u = this.hospitalMemoService.setDisabledHospitalList(expectedHospitalList);

    this.navCtrl.navigateForward(['hospitals']);
  }

  viewHospitalListForExpectedHospitals() {
    const hospital = this.myData.currentHospital as Hospital;
    if (hospital) { hospital.isSelected = true; }

    if (!this.myData || !this.myData.nurseExpectingHospitalList) { this.myData.nurseExpectingHospitalList = []; }

    const expectedHospitalList = this.myData.nurseExpectingHospitalList as Array<Hospital>;
    expectedHospitalList.map(h => h.isSelected = true);

    // const seletingHospitalList = expectedHospitalList;
    // if (hospital) { seletingHospitalList.push(hospital); }

    const r = this.hospitalMemoService.setMultiselectMode(true);
    const s = this.hospitalMemoService.setAllHospitalList(this.hospitalList);
    const t = this.hospitalMemoService.setSelectedHospitalList(expectedHospitalList);
    const u = this.hospitalMemoService.setDisabledHospitalList([hospital]);

    this.navCtrl.navigateForward(['hospitals']);
  }

  private initiateForm(): void {
    this.signupForm = this.formBuilder.group({
      fullName: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.pattern('^[a-zA-Z ]+$')
      ])),
      nic: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^([0-9]{9}[x|X|v|V]|[0-9]{12})$')
      ])),
      mobileNumber: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0][1-9][0-9]{8}$')
      ])),
      extraMobileNumber: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0][1-9][0-9]{8}$')
      ])),
      promoCode: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(6)
      ]))
    });
  }

  private getAllHospitals() {
    const parameterList = new Array<Parameter>();

    this.apiService.executeApiRequest('GetAllHospitals', parameterList).subscribe((res: Array<HospitalDto>) => {
      this.hospitalDtoList = res;
      this.hospitalList = res.map(h => ({
        id: h.id,
        district: h.district,
        name: h.name
      } as Hospital));
      console.log(res);
    });
  }

  private getMyData() {
    const parameterList = new Array<Parameter>();

    this.apiService.executeApiRequest('GetMyData', parameterList).subscribe((res: NurseDto) => {
      this.myData = res;
      if (this.myData.currentHospitalId > 0) {

      }

      this.signupForm.patchValue({
        fullName: this.myData.name,
        nic: this.myData.nic,
        mobileNumber: this.myData.mobileNumber,
        extraMobileNumber: this.myData.extraMobileNo,
        promoCode: this.myData.registeredPromoCode
      });
      console.log(res);
    });
  }

  private manageDataFromHospitalList(hospitalList: Array<Hospital>) {
    if (!this.hospitalMemoService.isInMultiSelectMode()) {
      // Thiis is the currentHospital
      this.myData.currentHospital = hospitalList[0];
      this.myData.currentHospitalId = (hospitalList[0] && hospitalList[0].id) ? hospitalList[0].id : 0;

      return;
    }

    this.myData.nurseExpectingHospitalList = hospitalList;
  }

  private initializeData(): void {
    const requestList: Array<any> = [];
    requestList.push(this.apiService.executeApiRequest('GetAllHospitals', []));
    if (this.isInEdit) { requestList.push(this.apiService.executeApiRequest('GetMyData', [])); }

    forkJoin(requestList).subscribe((resList: any[]) => {
      this.hospitalDtoList = resList[0];
      this.hospitalList = resList[0].map(h => ({
        id: h.id,
        district: h.district,
        name: h.name
      } as Hospital));

      //*********************************************/
      if (!this.isInEdit) { return; }

      this.myData = resList[1];
      if (this.myData.currentHospitalId > 0) {
        this.myData.currentHospital = this.hospitalList.find(h => h.id === this.myData.currentHospitalId);
      }

      this.signupForm.patchValue({
        fullName: this.myData.name,
        nic: this.myData.nic,
        mobileNumber: this.myData.mobileNumber,
        extraMobileNumber: this.myData.extraMobileNo,
        promoCode: this.myData.registeredPromoCode
      });
    });
  }

  private showSuccess() {
    this.navCtrl.navigateRoot('successmodal');
  }
}
