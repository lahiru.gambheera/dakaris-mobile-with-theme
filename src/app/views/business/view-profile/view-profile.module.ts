import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ViewProfilePageRoutingModule } from './view-profile-routing.module';
import { ViewProfilePage } from './view-profile.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { InfoModalPageModule } from '../modals/info-modal/info-modal.module';
import { InfoModalPage } from '../modals/info-modal/info-modal.page';



@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    IonicModule,
    ViewProfilePageRoutingModule,
    InfoModalPageModule
  ],
  entryComponents: [InfoModalPage],
  declarations: [ViewProfilePage],
  providers: []
})
export class ViewProfilePageModule { }
