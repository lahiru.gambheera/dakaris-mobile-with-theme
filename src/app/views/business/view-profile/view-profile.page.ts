import { GlobalApiService } from './../../../util/services/global-api.service';
import { NurseDto } from './../../../models/dto/nurse.model';
import { Component, OnInit } from '@angular/core';
import { ProfileDataAgentService } from 'src/app/shared/services/profile-data-agent.service';
import { District } from 'src/app/models/enum/district.enum';
import { Parameter } from 'src/app/util/model/api-service/parameter.model';
import { AlertController, ModalController } from '@ionic/angular';
import { InfoModalPage } from '../modals/info-modal/info-modal.page';
import { UnlockingContact } from 'src/app/models/dto/unlocking-contact.model';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.page.html',
  styleUrls: ['./view-profile.page.scss'],
})
export class ViewProfilePage implements OnInit {
  public nurse: NurseDto = {};
  private myAvailableViewCount = 0;
  public isContactUnlocked = false;

  constructor(
    private profileDataAgentService: ProfileDataAgentService,
    private apiService: GlobalApiService,
    private alertController: AlertController,
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
    const nurse = localStorage.getItem('nurse');
    this.nurse = JSON.parse(nurse);
    localStorage.removeItem('nurse');

    console.log('this.nurse', this.nurse);

    this.getMyAvailableViewCount();
  }

  onViewContactClick() {
    if (this.myAvailableViewCount < 1) {
      this.openInsufficientCreditsModal();
      return;
    }

    this.openConfirmationModal();
  }

  private openInsufficientCreditsModal() {
    this.modalCtrl.create({
      component: InfoModalPage,
      backdropDismiss: true,
      componentProps: {
        title: `Custom title`,
        message1: `You have no view credits to view this contact. Do you want to purchase view credits?`,
        button1Text: `View Packages`,
        button2Text: `Cancel`
      }
    }).then(modal => {
      modal.present();
      modal.onDidDismiss().then(res => {
        if (res.data.isButton1Clicked) {
          this.navigateToPackages();
        }
      });
    });
  }

  private openConfirmationModal(): void {
    this.modalCtrl.create({
      component: InfoModalPage,
      backdropDismiss: true,
      componentProps: {
        title: `Custom title`,
        message1: `You have ${this.myAvailableViewCount} of remaining view credits. Do you want to continue?`,
        button1Text: `Continue`,
        button2Text: `Cancel`
      }
    }).then(modal => {
      modal.present();
      modal.onDidDismiss().then(res => {
        if (res.data.isButton1Clicked) {
          this.getContactDataFrom();
        }
      });
    });
  }

  private openContacteeDontHaveViewCreditsModal() {

  }

  private openNotUnlockedModal(message: string) {
    this.modalCtrl.create({
      component: InfoModalPage,
      backdropDismiss: true,
      componentProps: {
        title: `Couldn't fetch contact details`,
        message1: message,
        button1Text: `OK`
      }
    }).then(modal => {
      modal.present();
      modal.onDidDismiss().then(res => {
        if (res.data.isButton1Clicked) {
          this.navigateToPackages();
        }
      });
    });
  }

  private navigateToPackages() {

  }

  private getContactDataFrom() {
    const parameterList = new Array<Parameter>();
    parameterList.push({
      name: 'id',
      value: this.nurse.encriptedId
    });

    this.apiService.executeApiRequest('UnlockFullContact', parameterList).subscribe((res: UnlockingContact) => {
      console.log('RES', res);
      if (!res.isUnlocked) {
        this.openNotUnlockedModal(res.message);
        return;
      }

      this.nurse.mobileNumber = res.contactee.phone1;
      this.nurse.extraMobileNo = res.contactee.phone2;
      this.isContactUnlocked = true;
    });
  }

  private getMyAvailableViewCount(): void {
    const parameterList = new Array<Parameter>();

    this.apiService.executeApiRequest('GetMyAvailableCreditCount', parameterList).subscribe((res: number) => {
      this.myAvailableViewCount = res;
    });
  }
}
