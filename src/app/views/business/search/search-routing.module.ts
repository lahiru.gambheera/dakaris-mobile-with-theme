import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TabsPage } from '../../reference/tabs/tabs.page';
import { SearchPage } from './search.page';


const routes: Routes = [
  // {
  //   path: '',
  //   component: SearchPage
  // }

  {
    path: '',
    component: SearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule { }
