import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchPage } from './search.page';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TabsPageModule } from '../../reference/tabs/tabs.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [SearchPage],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchRoutingModule,
    // TabsPageModule,
    SharedModule
  ],
  providers: []
})
export class SearchModule { }
