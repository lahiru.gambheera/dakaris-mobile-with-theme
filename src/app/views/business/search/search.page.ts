import { NurseExpectingHospitalList } from './../../../models/dto/nurse.model';
import { GlobalApiService } from './../../../util/services/global-api.service';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { IonSelect, NavController, ToastController } from '@ionic/angular';
import { District } from 'src/app/models/enum/district.enum';
import { Parameter } from 'src/app/util/model/api-service/parameter.model';
import { NurseDto } from 'src/app/models/dto/nurse.model';
import { ProfilePicService } from 'src/app/services/profile-pic.service';
import { ProfileDataAgentService } from 'src/app/shared/services/profile-data-agent.service';

@Component({
  selector: 'app-search.page',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  @ViewChild('districtSelect', { static: false }) districtSelect: IonSelect;

  private currentDistrictMode: 'from' | 'to' = 'from';

  public fromDistrictId = 0;
  public toDistrictId = 0;

  public districtList: Array<any>;

  public nurseList: Array<NurseDto> = [];

  public isLoadMoreEligible = false;
  private pageSize = 1;
  private pageNumberToBe = 0;

  constructor(
    private toastController: ToastController,
    private apiService: GlobalApiService,
    private profilePicService: ProfilePicService,
    private profileDataAgentService: ProfileDataAgentService,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    this.generateDistrictArray();
    localStorage.removeItem('nurse');
  }

  get fromDistrictName() {
    return District[this.fromDistrictId];
  }

  get toDistrictName() {
    return District[this.toDistrictId];
  }

  onOpenDistrictList(districtMode: 'from' | 'to') {
    this.currentDistrictMode = districtMode;
    this.districtSelect.open();
  }

  onDistrictChange(event: CustomEvent) {
    if (this.currentDistrictMode === 'from') {
      this.fromDistrictId = +event.detail.value;
      return;
    }

    if (this.currentDistrictMode === 'to') {
      this.toDistrictId = +event.detail.value;
      return;
    }
  }

  onSearchClicked() {
    if (!this.validateSearch()) {
      return;
    }

    this.nurseList = new Array();
    this.pageNumberToBe = 0;

    this.searchNurses(this.pageNumberToBe, this.pageSize);
  }

  onLoadMoreClick() {
    this.searchNurses(this.pageNumberToBe, this.pageSize);
  }

  onGotoProfileClich(nurse: NurseDto) {
    localStorage.setItem('nurse', JSON.stringify(nurse));
    this.profileDataAgentService.setNurseProfileData(nurse);
    this.navCtrl.navigateRoot('tabs/profile');
    // this.navCtrl.navigateRoot('tabs/view-profile');
  }

  private searchNurses(page: number, pageSize: number): void {
    const parameterList = new Array<Parameter>();
    parameterList.push({
      name: 'fromDistrict',
      value: this.fromDistrictId
    });
    parameterList.push({
      name: 'toDistrict',
      value: this.toDistrictId
    });
    parameterList.push({
      name: 'page',
      value: page
    });
    parameterList.push({
      name: 'pageSize',
      value: pageSize
    });

    this.apiService.executeApiRequest('searchNurse', parameterList).subscribe((res: Array<NurseDto>) => {
      this.isLoadMoreEligible = res.length === pageSize;

      res = res.map(n => {
        n.expectedHospitalDescription = this.prepareExpectedHospitalListText(n.nurseExpectingHospitalList);
        n.avatarUrl = this.profilePicService.getProfilePicUrl(n.encriptedId);

        return n;
      });

      res.forEach(n => { this.nurseList.push(n); });

      this.pageNumberToBe++;
    });
  }

  private generateDistrictArray() {
    const StringIsNumber = value => isNaN(Number(value)) === false;

    this.districtList = Object.keys(District)
      .filter(StringIsNumber)
      .map(key => ({ id: key, name: District[key] }));
  }

  private validateSearch() {
    if (this.fromDistrictId === 0 && this.toDistrictId === 0) {
      this.toastController.create({
        message: 'Please select both from and to districts',
        duration: 1500,
        animated: true,
        color: 'warning',
        buttons: [{ text: 'close' }]
      }).then(toast => {
        toast.present();
      });

      return false;
    }

    if (this.fromDistrictId !== 0 && this.toDistrictId === 0) {
      this.toastController.create({
        message: 'Please select to district',
        duration: 1500,
        animated: true,
        color: 'warning',
        buttons: [{ text: 'close' }]
      }).then(toast => {
        toast.present();
      });

      return false;
    }

    if (this.fromDistrictId === 0 && this.toDistrictId !== 0) {
      this.toastController.create({
        message: 'Please select from district',
        duration: 1500,
        animated: true,
        color: 'warning',
        buttons: [{ text: 'close' }]
      }).then(toast => {
        toast.present();
      });

      return false;
    }

    return true;
  }

  private prepareExpectedHospitalListText(nurseExpectingHospitalList: Array<NurseExpectingHospitalList>): string {
    const hospitalCount = nurseExpectingHospitalList.length;
    if (hospitalCount === 0) { return 'No hospitals recorded'; }

    let text = nurseExpectingHospitalList[0].hospitalName;

    if (hospitalCount > 1) {
      text = `${text} +${hospitalCount} more`;
    }

    return text;
  }
}
