import { ViewProfilePage } from './../view-profile/view-profile.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('../my-contact-list/my-contact-list.module').then(m => m.MyContactListPageModule)
          }
        ]
      },
      {
        path: 'tab2',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('../notifications/notifications.module').then(m => m.NotificationsPageModule)
          }
        ]
      },
      {
        path: 'view-profile',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('../profile/profile.module').then(m => m.ProfilePageModule)
          }
        ]
      },
      {
        path: 'tab3',
      children: [
          {
            path: '',
            loadChildren: () =>
            import('../search/search.module').then(m => m.SearchModule)
          }
        ]
      },
      {
        path: 'tab4',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('../packages/packages.module').then(m => m.PackagesPageModule)
          }
        ]
      },
      {
        path: 'tab5/:isInEdit',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('../signup/signup.module').then(m => m.SignupPageModule)
          }
        ]
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('../view-profile/view-profile.module').then(m => m.ViewProfilePageModule)
          }
        ]
      },
      // {
      //   path: '',
      //   redirectTo: '/tabs/tab1',
      //   pathMatch: 'full'
      // }
    ]
  },
  // {
  //   path: '',
  //   redirectTo: '/tabs/tab1',
  //   pathMatch: 'full'
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
