import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PackagesPageRoutingModule } from './packages-routing.module';

import { PackagesPage } from './packages.page';
import { SuccessmodalPage } from '../modals/successmodal/successmodal.page';
import { SuccessmodalPageModule } from '../modals/successmodal/successmodal.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    //SuccessmodalPageModule,
    PackagesPageRoutingModule
  ],
  entryComponents: [],
  declarations: [PackagesPage]
})
export class PackagesPageModule { }
