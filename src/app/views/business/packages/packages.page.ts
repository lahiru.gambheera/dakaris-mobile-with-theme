import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { Chart } from 'chart.js';
import * as crosshair from 'chartjs-plugin-crosshair';
import { Package } from 'src/app/models/dto/package.model';
import { Parameter } from 'src/app/util/model/api-service/parameter.model';
import { GlobalApiService } from 'src/app/util/services/global-api.service';
import { InfoModalPage } from '../modals/info-modal/info-modal.page';
import { SuccessmodalPage } from '../modals/successmodal/successmodal.page';

@Component({
  selector: 'app-packages',
  templateUrl: './packages.page.html',
  styleUrls: ['./packages.page.scss'],
})
export class PackagesPage implements OnInit {
  // @ViewChild('lineCanvas', {static: true}) lineCanvas;
  bars: any;
  colorArray: any;
  lineChart: any;

  packageList: Array<Package> = [];

  constructor(
    private navCtrl: NavController,
    private apiService: GlobalApiService,
    private modalCtrl: ModalController) { }

  ngOnInit() {
    this.getAllPackages();
  }

  logout() {
    this.navCtrl.navigateRoot('login');
  }

  onPackageRequest(packageId: number) {
    const parameterList = new Array<Parameter>();
    parameterList.push({
      name: 'packageId',
      value: packageId.toString()
    });

    this.apiService.executeApiRequest('RequestPackage', parameterList).subscribe((res: Array<boolean>) => {
      this.navCtrl.navigateRoot('successmodal');
    }, error => {

    });
  }

  getAllPackages() {
    const parameterList = new Array<Parameter>();

    this.apiService.executeApiRequest('GetAllPackages', parameterList).subscribe((res: Array<Package>) => {
      this.packageList = res.sort((a, b) => (a.price > b.price) ? 1 : -1);
    }, error => {

    });
  }
}
