import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { MyNotification } from 'src/app/models/dto/my-notification.model';
import { PreviouslyContactedNurse } from 'src/app/models/dto/unlocking-contact.model';
import { ProfilePicService } from 'src/app/services/profile-pic.service';
import { Parameter } from 'src/app/util/model/api-service/parameter.model';
import { GlobalApiService } from 'src/app/util/services/global-api.service';
import { InfoModalPage } from '../modals/info-modal/info-modal.page';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-about',
  templateUrl: './my-contact-list.page.html',
  styleUrls: ['./my-contact-list.page.scss'],
})
export class MyContactListPage implements OnInit {

  public myContactList: Array<PreviouslyContactedNurse> = [];

  constructor(
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    private apiService: GlobalApiService,
    private profilePicService: ProfilePicService,
    private callNumber: CallNumber) { }

  ngOnInit() {
    this.getMyMyContactList();
  }

  viewMyContactList() {
    this.navCtrl.navigateForward('MyContactList');
  }

  private getMyMyContactList() {

    const parameterList = new Array<Parameter>();

    this.apiService.executeApiRequest('GetMyContactedNurseList', parameterList).subscribe((res: Array<PreviouslyContactedNurse>) => {
      console.log(res);

      console.log('contact list', res);

      this.myContactList = res;

      res = res.map(p => {
        p.avatarUrl = this.profilePicService.getProfilePicUrl(p.encriptedId);

        this.onCallClick(p.phone1);

        return p;
      });

    });
  }

  onCallClick(phone: string) {
    console.log('PHONE', phone);
    debugger;
    this.callNumber.callNumber( phone, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }


}
