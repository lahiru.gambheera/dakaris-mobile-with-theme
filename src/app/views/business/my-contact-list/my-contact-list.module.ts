import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyContactListRoutingModule } from './my-contact-list-routing.module';
import { NgxGaugeModule } from 'ngx-gauge';
import { MyContactListPage } from './my-contact-list.page';

import { InfoModalPageModule } from '../modals/info-modal/info-modal.module';
import { InfoModalPage } from '../modals/info-modal/info-modal.page';
import { DakarisDatePipe } from 'src/app/util/pipes/dakaris-date.pipe';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    IonicModule,
    MyContactListRoutingModule,
    NgxGaugeModule,
    InfoModalPageModule
  ],
  entryComponents: [InfoModalPage],
  declarations: [MyContactListPage]
})
export class MyContactListPageModule {}