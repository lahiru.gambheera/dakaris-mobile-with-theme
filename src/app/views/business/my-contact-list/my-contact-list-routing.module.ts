import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyContactListPage } from './my-contact-list.page';

const routes: Routes = [
  {
    path: '',
    component: MyContactListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyContactListRoutingModule {}
