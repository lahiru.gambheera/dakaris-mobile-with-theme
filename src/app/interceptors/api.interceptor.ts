import { Injectable } from "@angular/core";
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from "@angular/common/http";
import { throwError, Observable, BehaviorSubject, of, from } from "rxjs";
import { catchError, filter, take, switchMap } from "rxjs/operators";
import { NativeStorage } from "@ionic-native/native-storage/ngx";

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
    private AUTH_HEADER = "Authorization";
    private token = "secrettoken";
    private refreshTokenInProgress = false;
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    constructor(private nativeStorage: NativeStorage) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!req.headers.has('Content-Type')) {
            req = req.clone({
                headers: req.headers.set('Content-Type', 'application/json')
            });
        }

        return from(this.nativeStorage.getItem('auth_token'))
            .pipe(
                switchMap(token => {
                    // Token is available. Can use token from here onwards.
                    const headers = req.headers
                        .set('Authorization', 'Bearer ' + token)
                        .append('Content-Type', 'application/json');

                    const requestClone = req.clone({ headers });

                    return next.handle(requestClone);
                })
                , catchError(e => {
                    // Token is not available. Need to proceed without a token.
                    return next.handle(req);
                }))
    }

    private refreshAccessToken(): Observable<any> {
        return of("secret token");
    }
}