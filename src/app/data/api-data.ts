import { ApiRequest } from '../util/model/api-service/apiRequest.model';

export class ApiData {

  public static getData(): Array<ApiRequest> {
    const data = [
      {
        path: '/addresses',
        version: 'v1',
        httpRequest: 'POST',
        operationId: 'createUsingPOST',
        requestTypeList: [
          {
            name: 'resRequest',
            in: 'body',
            required: true,
            type: 'AppRequest<AddressRequestChild1>'
          }
        ],
        responseType: 'AppResponse<AddressResponse>'
      },
      {
        path: '/addresses/{id}',
        version: 'v1',
        httpRequest: 'GET',
        operationId: 'readByIdUsingGET',
        requestTypeList: [
          {
            name: 'id',
            in: 'path',
            required: true,
            type: 'number'
          }
        ],
        responseType: 'AppResponse<AddressResponse>'
      },
      {
        path: 'api/Nurse/Search/{fromDistrict}/{toDistrict}/{page}/{pageSize}',
        version: 'v1',
        httpRequest: 'GET',
        operationId: 'searchNurse',
        requestTypeList: [
          {
            name: 'fromDistrict',
            in: 'path',
            required: true,
            type: 'number'
          },
          {
            name: 'toDistrict',
            in: 'path',
            required: true,
            type: 'number'
          },
          {
            name: 'page',
            in: 'path',
            required: true,
            type: 'number'
          },
          {
            name: 'pageSize',
            in: 'path',
            required: true,
            type: 'number'
          }
        ],
        responseType: 'Array<Nurse>'
      },
      {
        path: 'api/Nurse/GetMyAvailableCreditCount',
        version: 'v1',
        httpRequest: 'GET',
        operationId: 'GetMyAvailableCreditCount',
        requestTypeList: [],
        responseType: 'AppResponse<number>'
      },
      {
        path: 'api/Nurse/UnlockFullContact/{id}',
        version: 'v1',
        httpRequest: 'GET',
        operationId: 'UnlockFullContact',
        requestTypeList: [
          {
            name: 'id',
            in: 'path',
            required: true,
            type: 'string'
          }
        ],
        responseType: 'AppResponse<UnlockingContact>'
      },
      {
        path: 'api/Package/{packageId}',
        version: 'v1',
        httpRequest: 'POST',
        operationId: 'RequestPackage',
        requestTypeList: [
          {
            name: 'packageId',
            in: 'path',
            required: true,
            type: 'string'
          }
        ],
        responseType: 'AppResponse<boolean>'
      },
      {
        path: 'api/Package/GetAllPackages',
        version: 'v1',
        httpRequest: 'GET',
        operationId: 'GetAllPackages',
        requestTypeList: [],
        responseType: 'AppResponse<Array<Package>>'
      },
      {
        path: 'api/Hospital',
        version: 'v1',
        httpRequest: 'GET',
        operationId: 'GetAllHospitals',
        requestTypeList: [],
        responseType: 'AppResponse<HospitalDto>'
      },
      {
        path: 'api/Nurse/GetMyData',
        version: 'v1',
        httpRequest: 'GET',
        operationId: 'GetMyData',
        requestTypeList: [],
        responseType: 'AppResponse<NurseDto>'
      },
      {
        path: 'api/Nurse/PostNurse',
        version: 'v1',
        httpRequest: 'POST',
        operationId: 'PostNurse',
        requestTypeList: [
          {
            name: 'nurse',
            in: 'body',
            required: true,
            type: 'NurseDto'
          }
        ],
        responseType: 'any'
      },
      {
        path: 'api/Login/GoogleLogin',
        version: 'v1',
        httpRequest: 'POST',
        operationId: 'GoogleLogin',
        requestTypeList: [
          {
            name: 'googleUser',
            in: 'body',
            required: true,
            type: 'any'
          }
        ],
        responseType: 'AppResponse<any>'
      },
      {
        path: 'api/Notification/MyNotifications/{page}/{pageSize}',
        version: 'v1',
        httpRequest: 'GET',
        operationId: 'MyNotifications',
        requestTypeList: [
          {
            name: 'page',
            in: 'path',
            required: true,
            type: 'string'
          },
          {
            name: 'pageSize',
            in: 'path',
            required: true,
            type: 'string'
          }
        ],
        responseType: 'AppResponse<any>'
      },
      {
        path: 'api/Nurse/GetMyContactedNurseList',
        version: 'v1',
        httpRequest: 'GET',
        operationId: 'GetMyContactedNurseList',
        requestTypeList: [ ],
        responseType: 'AppResponse<PreviouslyContactedNurse>'
      }
    ];

    return data;
  }
}
