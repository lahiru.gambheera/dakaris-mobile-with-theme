
export class BaseUrlData {
    public static getData() {
        const data = {
            dev: {
                BASE_DATA_URL: 'https://localhost:44336'
            },
            prod: {
                BASE_DATA_URL: 'https://localhost:44336'
            }
        };

        return data;
    }
}
