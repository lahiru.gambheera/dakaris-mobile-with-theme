import { browser } from 'protractor';
import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free/ngx';
// import { SqlProvider } from './sql/sql.provider';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  dark = false;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    // private sql: SqlProvider,
    public admobFree: AdMobFree
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if (this.platform.is("cordova")) {
        this.showBannerAd();
      }
    });

    // this.initDb();
  }

  showBannerAd() {
    let bannerConfig: AdMobFreeBannerConfig = {
      isTesting: false, // Remove in production
      autoShow: true,
      id: "ca-app-pub-2582975357316139/8583143350"
    };
    this.admobFree.banner.config(bannerConfig);

    this.admobFree.banner.prepare().then(() => {
      // success
    }).catch(e => alert(e));
  }

  async initDb() {
    // await this.sql.dbInstance.executeSql('CREATE TABLE IF NOT EXISTS user(id INTEGER PRIMARY KEY, name)');
    // await this.sql.dbInstance.executeSql(`INSERT INTO user(id, user) VALUES (1, 'Suraj')`);
    // let users = await this.sql.dbInstance.executeSql('SELECT * FROM user');
    // console.log(users);
  }
}
