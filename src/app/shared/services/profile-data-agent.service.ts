import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { NurseDto } from '../../models/dto/nurse.model';

@Injectable({
    providedIn: 'root'
})
export class ProfileDataAgentService {
    public nurseProfileData: NurseDto = {};
    // public subject = new Subject<any>();

    private messageSource = new BehaviorSubject(this.nurseProfileData);

    public currentNurseProfileData = this.messageSource.asObservable();

    setNurseProfileData(nurse: NurseDto) { this.messageSource.next(nurse); }
}
