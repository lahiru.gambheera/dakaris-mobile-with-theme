import { Pipe, PipeTransform } from '@angular/core';
import { District } from 'src/app/models/enum/district.enum';

@Pipe({name: 'district'})
export class DistrictPipe implements PipeTransform {
  transform(value: string): string {
    return District[+value];
  }
}