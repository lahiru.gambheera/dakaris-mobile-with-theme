import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileDataAgentService } from './services/profile-data-agent.service';
import { DistrictPipe } from './pipes/district.pipe';
import { DakarisDatePipe } from '../util/pipes/dakaris-date.pipe';



@NgModule({
  declarations: [DistrictPipe, DakarisDatePipe],
  imports: [
    CommonModule
  ],
  providers: [ProfileDataAgentService],
  exports: [DistrictPipe, DakarisDatePipe]
})
export class SharedModule { }
