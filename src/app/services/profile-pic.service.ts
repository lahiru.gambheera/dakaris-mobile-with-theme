import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class ProfilePicService {
    public getProfilePicUrl(id: string): string {
        const aschii = id.charCodeAt(12);
        const picIndex = aschii % 36;

        let picUrl = `assets/female-avatar/`;

        if (picIndex < 10) {
            picUrl = `${picUrl}00${picIndex}-avatar.svg`;
            return picUrl;
        }

        picUrl = `${picUrl}0${picIndex}-avatar.svg`;
        return picUrl;
    }
}
