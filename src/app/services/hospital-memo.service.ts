import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Hospital } from '../models/business/hospital.model';

@Injectable({
  providedIn: 'root'
})
export class HospitalMemoService {

  private selectedHospitalList: Array<Hospital>;
  private disabledHospitalList: Array<Hospital>;
  private allHospitalList: Array<Hospital>;
  private multiSelectMode = false;
  private isInEdit = true;

  private selectedCurrentHospitalDataSource = new BehaviorSubject<Array<Hospital>>(new Array<Hospital>());
  public selectedCurrentHospitaldata = this.selectedCurrentHospitalDataSource.asObservable();

  private selectedExpectedHospitalsDataSource = new BehaviorSubject<Array<Hospital>>(new Array<Hospital>());
  public selectedExpectedHospitalsdata = this.selectedExpectedHospitalsDataSource.asObservable();

  constructor() { }

  public getAllHospitalList(): Array<Hospital> {
    return this.allHospitalList;
  }

  public setAllHospitalList(hospitalList: Array<Hospital>): boolean {
    this.allHospitalList = hospitalList;
    return true;
  }

  public getSelectedHospitalList(): Array<Hospital> {
    return this.selectedHospitalList;
  }

  public setSelectedHospitalList(hospitalList: Array<Hospital>): boolean {
    this.selectedHospitalList = new Array<Hospital>();

    hospitalList.forEach(h => this.selectedHospitalList.push(h));

    return true;
  }

  public getDisabledHospitalList(): Array<Hospital> {
    return this.disabledHospitalList;
  }

  public setDisabledHospitalList(hospitalList: Array<Hospital>): boolean {
    this.disabledHospitalList = new Array<Hospital>();

    hospitalList.forEach(h => this.disabledHospitalList.push(h));

    return true;
  }

  public setIsInEditState(isInEdit: boolean): void {
    this.isInEdit = isInEdit;
  }

  public getIsInEditState(): boolean {
    return this.isInEdit;
  }

  // public setSelectedHospital(hospital: Hospital): boolean {
  //   this.selectedHospitalList = new Array<Hospital>();
  //   this.selectedHospitalList.push(hospital);
  //   return true;
  // }

  // public getSelectedHospital(): Hospital {
  //   if (!this.selectedHospitalList.length || this.selectedHospitalList.length === 0) { return null; }

  //   return this.selectedHospitalList[0];
  // }

  public clearData(): boolean {
    this.selectedHospitalList = new Array<Hospital>();
    return true;
  }

  // public changeMultiSelectMode(isEnableMutiSelect: boolean): void {
  //   this.multiSelectMode = isEnableMutiSelect;
  // }

  public setMultiselectMode(isMultiSelect: boolean) {
    this.multiSelectMode = isMultiSelect;
    return true;
  }

  public isInMultiSelectMode(): boolean {
    return this.multiSelectMode;
  }

  public updatedCurrentHospitalDataSelection(data: Array<Hospital>) {
    this.selectedCurrentHospitalDataSource.next(data);
  }

  public updatedExpectedHospitalsDataSelection(data: Array<Hospital>) {
    this.selectedExpectedHospitalsDataSource.next(data);
  }
}
