import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./views/business/intro/intro.module').then( m => m.IntroPageModule)
  },
  {
    path: '',
    loadChildren: () => import('./views/business/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'intro',
    loadChildren: () => import('./views/business/intro/intro.module').then( m => m.IntroPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./views/reference/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'expenses',
    loadChildren: () => import('./views/reference/expenses/expenses.module').then( m => m.ExpensesPageModule)
  },
  {
    path: 'wallet',
    loadChildren: () => import('./views/reference/wallet/wallet.module').then( m => m.WalletPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./views/reference/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./views/reference/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signup/:isInEdit',
    loadChildren: () => import('./views/reference/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('./views/reference/notifications/notifications.module').then( m => m.NotificationsPageModule)
  },
  // {
  //   path: 'addtransactions',
  //   loadChildren: () => import('./views/reference/modals/info-modal/addtransactions.module').then( m => m.AddtransactionsPageModule)
  // },
  {
    path: 'request',
    loadChildren: () => import('./views/reference/request/request.module').then( m => m.RequestPageModule)
  },
  {
    path: 'requestmoney',
    loadChildren: () => import('./views/reference/requestmoney/requestmoney.module').then( m => m.RequestmoneyPageModule)
  },
  {
    path: 'requestreview',
    loadChildren: () => import('./views/reference/requestreview/requestreview.module').then( m => m.RequestreviewPageModule)
  },
  // {
  //   path: 'successmodal',
  //   loadChildren: () => import('./views/reference/modals/successmodal/successmodal.module').then( m => m.SuccessmodalPageModule)
  // },
  {
    path: 'addincome',
    loadChildren: () => import('./views/reference/addincome/addincome.module').then( m => m.AddincomePageModule)
  },

  // ---------------------------------------------------

  {
    path: 'search',
    loadChildren: () => import('./views/business/search/search.module').then( m => m.SearchModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./views/business/intro/intro.module').then( m => m.IntroPageModule)
  },
  {
    path: 'hospitals',
    loadChildren: () => import('./views/business/hospitals/hospitals.module').then( m => m.HospitalsPageModule)
  },
  {
    path: 'successmodal',
    loadChildren: () => import('./views/business/modals/successmodal/successmodal.module').then( m => m.SuccessmodalPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
