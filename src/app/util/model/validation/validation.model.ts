export interface Validation {
    code?: string;
    validationList?: Array<ValidationContent>;
}

export interface ValidationContent {
    validationType?: string; // 'required' | 'min-length' | 'max-length' | 'pattern' | 'email' | 'alpha-numeric' | 'max' | 'min' | 'number';
    errorMessage?: string;
    limit?: number | string;
}
