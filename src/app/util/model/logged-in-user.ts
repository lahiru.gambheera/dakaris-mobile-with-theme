export interface LoggedInUser {
    id: string;
    username: string;
    displayName: string;
    designation: string;
    roleId: string;
    roleName: string;
    divitionalSecetariat: string;
    dsId: string;
    permissions: string;
}
