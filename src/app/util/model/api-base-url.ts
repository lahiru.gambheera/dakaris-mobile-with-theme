export interface ApiBaseUrl {
    code?: string;
    version?: string;
    url?: string;
}
