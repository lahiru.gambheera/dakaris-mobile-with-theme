export interface AuthenticationDetails {
    accessTokenName: string;
    refereshToken: string;
    tokenType: string;
}
