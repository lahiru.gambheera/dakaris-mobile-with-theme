export interface ServerInBuiltError {
    error?: string;
    message?: string;
    path?: string;
    status?: number;
    timestamp?: Date;
}