
export interface ApiRequest {
    operationId?: string;
    version?: string;
    httpRequest?: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH' | string;
    requestTypeList?: Array<RequestType>;
    responseType?: string;
    path?: string;
}

export interface RequestType {
    name?: string;
    in?: 'path' | 'body' | 'query' | 'formData' | string;
    required?: boolean;
    type?: string;
}

