export interface AccessDetails {
    token: string;
    removeToken: string;
    clientDetails: string;
}

