import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalApiService } from './services/global-api.service';



@NgModule({
  declarations: [],
  imports: [],
  providers: [GlobalApiService]
})
export class UtilModule { }
