import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/internal/operators/catchError';
import { ApiRequest, RequestType } from '../model/api-service/apiRequest.model';
import { Parameter } from '../model/api-service/parameter.model';

import { AppResponse } from '../model/response-models/app-response';
import { ServerInBuiltError } from '../model/api-service/ServerInBuiltError.model';
import { AppRequest } from '../model/request-models/app-request';

import { ApiData } from 'src/app/data/api-data';
import { BaseUrlData } from 'src/app/data/base-url-data';
import { EnviromentConfigData } from 'src/app/data/enviroment-config-data';


@Injectable({ providedIn: 'root' })

export class GlobalApiService {

  private defaultVersion = 'v1';
  private queryCount = 0;

  constructor(private httpClient: HttpClient) { }

  public executeApiRequest<T>(
    urlCode: string,
    parameterList: Array<Parameter>,
    isMeta?: boolean,
    version?: string): Observable<any | ServerInBuiltError> {
    let name: string;
    let value: any;
    let parameter: Parameter;
    let bodyData: {};

    // const api = ApiData.getData();

    version = 'v1';

    // const apiRequestInfo = isMeta ? urlInfoMeta : urlInfoBu;

    // tslint:disable-next-line: no-string-literal
    const urlDetails = ApiData.getData() // apiRequestInfo['urlInfo']
      .find(urlInfo => urlInfo.operationId === urlCode && urlInfo.version === version) as ApiRequest;
    let url = urlDetails.path;
    const httpRequest = urlDetails.httpRequest;

    if (!this.propertiesValidation(urlDetails.requestTypeList, parameterList)) {
      window.alert(`Parameters you entered are not valid!, Try again with valid parameters`);
      return null;
    }

    let formData = new FormData();
    urlDetails.requestTypeList.forEach(requestType => {

      parameter = parameterList
        // tslint:disable-next-line: no-shadowed-variable
        .find(parameter => parameter.name === requestType.name) as Parameter;

      if (!parameter && requestType.required) {
        window.alert(`Parameter "${requestType.name}" is required!, Try again with "${requestType.name}"`);
      } else if (!parameter && !requestType.required) {
        // continue;
      } else {
        name = parameter.name;
        value = parameter.value;

        let expression: 'path' | 'body' | 'query' | 'formData' | string;
        expression = requestType.in;

        switch (expression) {
          case 'body': {
            bodyData = value;
            break;
          }
          case 'path': {
            url = url.replace(`{${name}}`, value);
            break;
          }
          case 'query': {
            url = this.setQuery(name, value, url);
            break;
          }
          case 'formData': {
            formData = this.setFormData(requestType, parameter, formData);
            break;
          }
          default: {
            break;
          }
        }
      }

      parameter = undefined;
    });
    this.queryCount = 0;

    const getBaseUrl = this.getBaseUrl(isMeta);

    url = `${getBaseUrl}/${url}`;

    // const appRequest: AppRequest<any> = { data: bodyData };
    const appRequest = bodyData;

    return this.httpClient
      .request<T>(httpRequest, url, { body: bodyData === undefined ? formData : appRequest, responseType: 'json' })
      .pipe(
        catchError((err: HttpErrorResponse) => {

          if (err.error instanceof ErrorEvent) {
            return of<any>({
              error: {
                title: 'Network error!',
                detail: err.message,
                source: err.statusText,
                status: err.status.toString()
              },
              meta: {
                code: err.status,
                message: err.message
              }
            });
          }

          return of<ServerInBuiltError>(err.error);
        }));
  }

  private setQuery(name: string, value: any, url: string): string {

    url = (this.queryCount === 0) ? `${url}?${name}=${value}` : `${url}&${name}=${value}`;
    this.queryCount++;
    return url;
  }

  private getBaseUrl(isMeta: boolean): string {
    const mode = EnviromentConfigData.getData().CURRENT_MODE;

    const baseUrlBlock = BaseUrlData.getData()[mode];
    const baseUrl = isMeta ? baseUrlBlock.BASE_META_URL : baseUrlBlock.BASE_DATA_URL;

    return baseUrl;
  }

  private propertiesValidation(requestType: Array<RequestType>, parameterList: Array<Parameter>): boolean {
    // tslint:disable-next-line: variable-name
    const request_type = new Array<any>();
    // tslint:disable-next-line: variable-name
    const parameter_name = new Array<any>();

    requestType.forEach(rType => {
      request_type.push(rType.name);
    });

    parameterList.forEach(parameterName => {
      parameter_name.push(parameterName.name);

    });

    const result = parameter_name.every(val => request_type.includes(val));
    return result;
  }

  private setFormData(requestType: RequestType, parameter: Parameter, formData: FormData): FormData {
    if (requestType.type === 'array') {
      for (let i = 0; parameter.value.length > i; i++) {
        formData.append(parameter.name, parameter.value[i], parameter.value[i].name);
      }
    } else {
      formData.append(parameter.name, parameter.value);
    }
    return formData;
  }
}
