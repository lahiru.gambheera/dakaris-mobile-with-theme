import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';


@Pipe({
    name: 'dakarisDate'
})
export class DakarisDatePipe extends DatePipe implements PipeTransform {

    DATEFORMATLIST = [
        {
            "code": "default",
            "format": "dd/MM/yyyy"
        },
        {
            "code": "table",
            "format": "dd/MM/yy"
        },
        {
            "code": "label",
            "format": "dd/MMM/yyyy"
        },
        {
            "code": "table-date-time",
            "format": "dd/MM/yy HH:mm"
        },
        {
            "code": "label-date-time",
            "format": "dd/MMM/yyyy HH:mm"
        },
        {
            "code": "year-month",
            "format": "MMM/yyyy"
        }
    ];

    transform(value: string | Date, code: string): string {
        let dateObject = this.DATEFORMATLIST
            .find(url => url.code === code);

        // dateObject = dateObject ? dateObject : dateFormatList.DATEFORMATLIST.find(url => url.code === 'default');

        if (!dateObject) {
            dateObject = this.DATEFORMATLIST.find(url => url.code === 'default');
        }


        const formatedDate = super.transform(value, dateObject.format);
        return formatedDate;

    }

}


