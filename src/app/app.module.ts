import { HttpClient, HttpClientModule, HttpHandler, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UtilModule } from './util/util.module';
import { SuccessmodalPageModule } from './views/reference/modals/successmodal/successmodal.module';
// import { AddtransactionsPageModule } from './views/reference/modals/info-modal/addtransactions.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdMobFree } from '@ionic-native/admob-free/ngx';
import { AddtransactionsPageModule } from './views/reference/modals/addtransactions/addtransactions.module';
import { HospitalMemoService } from './services/hospital-memo.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { ApiInterceptor } from './interceptors/api.interceptor';
import { DakarisDatePipe } from './util/pipes/dakaris-date.pipe';
import { CallNumber } from '@ionic-native/call-number/ngx';
// import { SqlProvider } from './sql/sql.provider';
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    UtilModule,
    AddtransactionsPageModule,
    SuccessmodalPageModule],
  providers: [
    HttpClient,
    StatusBar,
    SplashScreen,
    AdMobFree,
    HospitalMemoService,
    NativeStorage,
    // SqlProvider,
    { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    CallNumber
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
