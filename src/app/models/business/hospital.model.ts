import { District } from '../enum/district.enum';

export interface Hospital {
    id?: number;
    name?: string;
    district?: District;
    isSelected?: boolean;
    isDisabled?: boolean;
}
