export interface GoogleUser {
    name?: string;
    email?: string;
    userId?: string;
    token?: string;
}