export interface CurrentHospital {
    id?: number;
    name?: string;
    district?: number;
    isActive?: boolean;
}

export interface NurseExpectingHospitalList {
    id?: number;
    nurseId?: number;
    hospitalId?: number;
    hospitalName?: string;
    district?: number;
}

export interface NurseDto {
    id?: number;
    encriptedId?: string;
    name?: string;
    nic?: string;
    mobileNumber?: string;
    extraMobileNo?: string;
    email?: string;
    profilePic?: string;
    myPromoCode?: string;
    registeredPromoCode?: string;
    hasPaidForPromo?: boolean;
    currentHospitalId?: number;
    expectedHospitalDescription?: string;
    packageId?: number;
    availableViewCredits?: number;
    isActive?: boolean;
    isSuspended?: boolean;
    avatarUrl?: any;
    package?: any;
    isViewCreditsAvailable?: boolean;
    viewCount?: number;

    currentHospital?: CurrentHospital;
    nurseExpectingHospitalList?: NurseExpectingHospitalList[];
}