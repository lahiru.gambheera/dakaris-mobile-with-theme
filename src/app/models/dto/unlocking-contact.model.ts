export interface UnlockingContact {
    isUnlocked?: boolean;
    message?: string;
    contactee?: PreviouslyContactedNurse;
}

export interface PreviouslyContactedNurse {
    encriptedId?: string;
    name?: string;
    phone1?: string;
    phone2?: string;
    currentHospital?: any;
    expectedHospitalList?: any;

    avatarUrl?: any;
}
