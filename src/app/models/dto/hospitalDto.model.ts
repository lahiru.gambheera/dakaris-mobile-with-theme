import { District } from '../enum/district.enum';

export interface HospitalDto {
    id?: number;
    name?: string;
    district?: District;
    isActive?: boolean;
}
