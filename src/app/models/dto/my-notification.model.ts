export interface MyNotification {
    id?: string;
    nurseId?: string;
    time?: string;
    message?: string;
    isRead?: string;
    isActionButtonAvailable?: string;
    actionButtonText?: string;
    actionButtonLink?: string;
}