export interface LoginSuccessUser {
    token?: string;
    isRegistrationCompleted?: boolean;
    userId?: number;
}