export interface Package {
    id?: number;
    name?: string;
    code?: string;
    price?: number;
    description?: string;
    maxViewCount?: number;
    isActive?: boolean;
}